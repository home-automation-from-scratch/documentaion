# Documentation

## Goal
The goal of this project is to create an easy to setup, local, **home automation hub**.  
The user must be able to download configure and run the service, and then deploy all the
end devices he/she wants.

## Infrastructure
The main services are an **Event Manager**, connected to a **Database** and a **HTTP Broker** for the IOT Devices.  
For the user interface a **Telegram Bot** and a **Web Interface** is deployed.

Every service communicates to the Event Manager via HTTP Requests.