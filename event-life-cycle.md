# Event Life Cycle

This document describes the life cycle of the supported events.

1. __Sensor Data Logging__
  * The microcontroller end-device reads a value of its sensor.  
  * The end-device posts the data to the HTTP Endpoint.  
  (data is post in json format with the fields "location", "sensor", "value")  
  (the routes for posting data is /postData and /postUrgent)
  * The HTTP Endpoint posts the data, adding the key "date-time", to the Event Manager.
  (the route for inserting data to the DB is /insertData)  
  * Finally, Event Manager stores the data to the DB.  

 Later on, authentication of the end-devices and data validation will be added to the HTTP Broker.

2. __Sensor Data Retrieval__
  * A service (Web Interface or Telegram Bot) can request (GET Request) from the Event Manager sensor data.
  (The fields requested are "location" and "sensor")
  * The Event Manager returns the "value" and "date-time" fields of the latest corresponding document.

 Still in concept stage, no routes configured.
 Later on, the ability to retrieve multiple documents will be added (by number, date-time range or other filters).

3. __New Actuator Deployed__
  * The end-device has its preconfigured fields "location", "actuator", "range" : "min", "max".
  * The end-device posts the fields above to the HTTP Endpoint on deployment (connection to WiFi network).
  * The HTTP Endpoint reads the device's IP address.
  * The HTTP Endpoint posts the data received with the additional field of "ip" to the Event manager.
  * The Event Manager checks for documents with the same "location" and "actuator" fields, updating those documents with the new values, otherwise inserts the new document.

 Still in concept stage, no routes configured.

4. __Actuator Activation__
  * The Event Manager receives a POST request (from Telegram Bot or Web Interface) with the "location", "actuator" and "value".
  * The Event Manager retrieves the "ip" for the given location-actuator key and posts to the HTTP Endpoint the received document, with the "ip" field added to it.
  * The HTTP Endpoint posts the document to the end-device.

5. __Automated Actuation__  
There will be two ways of automation.  
i. Incoming Sensor Data can trigger a new event, for example a low room temp could turn up the heating.  
ii. User can set scheduled events, for example turn on the lights in an hour, or every day at 8.00.
