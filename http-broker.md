# HTTP Broker

The HTTP Broker communicates with the end-devices.  
It receives incoming sensor data and enables the actuators.  
It also communicates with the event manager to store the data received and also receive the values for the actuator output.  
It's the service responsible for filtering out the incoming data and later on authenticating the devices.  


## URL Routes
- /postData is the URL used by the end-devices to POST data to the Broker. The data must be in json format and must contain "location", "sensor" and "value" fields.  
- /postUrgent is the URL used by the end-devices to POST data to the Broker. These are sensitive data that the user has to be notified immediately about. The key-value pair {"urgent":"true"} is added.

## Posting data to Event Manager
The HTTP Broker posts the incoming data to the Event Manager.  
Data contain the additional field of "time-date".  
