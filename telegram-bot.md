#Telegram Bot

To setup your telegram bot visit this site:  
https://core.telegram.org/bots  
It will take you through the process of creating a bot, using BotFather, and the give you yours bot *token*.  
  
On the script you shall input that token and the bot will be ready for use.  
  
Also, be sure to download the latest *beta* version via pip, or else the script won't run.  
  
The bot uses the "/set" command to activate an actuator. Following the command there are three arguments, "location", "actuator", "value". For example the message "/set bedroom lights off" is used to turn off the bedroom lights.  
The bot is only filtering data to count the number of arguments.  
Any confirmation of the actuator changing state will be shown to the user if the end-device script POST its state as urgent data (see HTTP Handler).  
  
-/help command answers with the commands available for the bot.  
-/test command answers with the Chat ID and message send.

The bot also listens to a port and can receive POST requests, on the /notify route, with text content, that will be sent to certain chats. The chat ids will be retrieved from the DB via the event manager (right now one id is written in code).  

